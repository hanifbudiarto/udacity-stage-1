package com.hanifbudiarto.stage1.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Hanif on 7/1/2017.
 */

public class DateUtils {

    public static String formatDate(String dateStr) {
        return formatDate(dateStr, "yyyy-MM-dd", "dd MMM yyyy");
    }

    public static String formatDate(String dateStr, String fromPattern, String toPattern) {
        SimpleDateFormat sdfFrom = new SimpleDateFormat(fromPattern);
        try {
            Date date = sdfFrom.parse(dateStr);

            SimpleDateFormat sdfTo = new SimpleDateFormat(toPattern);
            return sdfTo.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateStr;
    }
}
