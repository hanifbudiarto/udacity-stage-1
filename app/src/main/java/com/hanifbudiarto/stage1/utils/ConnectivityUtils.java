package com.hanifbudiarto.stage1.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Hanif on 7/1/2017.
 */

public class ConnectivityUtils {

    public static boolean isOnline(Activity activity ) {
        ConnectivityManager connectivityManager =
                ( ConnectivityManager ) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
