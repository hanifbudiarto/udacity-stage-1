package com.hanifbudiarto.stage1.display.reviews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hanifbudiarto.stage1.R;
import com.hanifbudiarto.stage1.data.review.Review;
import com.hanifbudiarto.stage1.display.ClickListener;

import java.util.List;

/**
 * Created by Hanif on 7/30/2017.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewHolder> {

    private ClickListener mListener;

    private boolean mShouldAttachToParentImmediately = false;
    private Context mContext;
    private int mResource;
    private List<Review> mReviews;

    public ReviewsAdapter(Context context, int resource, List<Review> reviewList) {
        this.mContext = context;
        this.mResource = resource;
        this.mReviews = reviewList;
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mResource, parent, mShouldAttachToParentImmediately);
        return new ReviewsAdapter.ReviewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReviewHolder holder, int position) {

        Review review = mReviews.get(position);

        holder.mAuthor.setText(review.getAuthor());
        holder.mContent.setText(review.getContent());
    }

    @Override
    public int getItemCount() {
        return mReviews.size();
    }

    public class ReviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mAuthor, mContent, mMore;

        public ReviewHolder(View itemView) {
            super(itemView);

            mAuthor = itemView.findViewById(R.id.tv_author);
            mContent = itemView.findViewById(R.id.tv_review);
            mMore = itemView.findViewById(R.id.tv_more);

            mMore.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            mListener.onClick(getAdapterPosition());
        }
    }

    public void setReviewsListener(ClickListener listener) {
        this.mListener = listener;
    }
}
