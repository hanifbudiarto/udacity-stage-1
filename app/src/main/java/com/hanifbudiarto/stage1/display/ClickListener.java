package com.hanifbudiarto.stage1.display;

/**
 * Created by Hanif on 7/29/2017.
 */

public interface ClickListener {
    void onClick(int index);
}
