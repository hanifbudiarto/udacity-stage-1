package com.hanifbudiarto.stage1.display.thumbnail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hanifbudiarto.stage1.R;
import com.hanifbudiarto.stage1.api.youtube.YoutubeThumbnail;
import com.hanifbudiarto.stage1.data.trailer.Trailer;
import com.hanifbudiarto.stage1.display.ClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Hanif on 7/29/2017.
 */

public class ThumbnailsAdapter extends RecyclerView.Adapter<ThumbnailsAdapter.ThumbnailHolder> {

    private final String TAG = getClass().getSimpleName();

    private ClickListener mListener;

    private boolean mShouldAttachToParentImmediately = false;
    private Context mContext;
    private int mResource;
    private List<Trailer> mTrailers;

    public ThumbnailsAdapter(Context context, int resource, List<Trailer> trailerList) {
        this.mContext = context;
        this.mResource = resource;
        this.mTrailers = trailerList;
    }

    @Override
    public ThumbnailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mResource, parent, mShouldAttachToParentImmediately);
        return new ThumbnailHolder(view);
    }

    @Override
    public void onBindViewHolder(ThumbnailHolder holder, int position) {
        String thumbnailPath = YoutubeThumbnail.getImage(mTrailers.get(position).getKey());
        Log.e(TAG, thumbnailPath);

        Picasso.with(mContext)
                .load(thumbnailPath)
                .into(holder.mIvThumbnail);
    }

    @Override
    public int getItemCount() {
        return mTrailers.size();
    }

    public void setThumbnailListener(ClickListener listener) {
        this.mListener = listener;
    }

    public class ThumbnailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView mIvThumbnail;

        public ThumbnailHolder(View itemView) {
            super(itemView);
            mIvThumbnail = itemView.findViewById(R.id.iv_thumbnail);

            mIvThumbnail.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(getAdapterPosition());
        }
    }
}
