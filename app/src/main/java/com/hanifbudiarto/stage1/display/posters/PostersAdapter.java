package com.hanifbudiarto.stage1.display.posters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hanifbudiarto.stage1.R;
import com.hanifbudiarto.stage1.api.tmdb.helper.TMDBHelper;
import com.hanifbudiarto.stage1.data.movie.Movie;
import com.hanifbudiarto.stage1.display.ClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Hanif on 7/1/2017.
 */

public class PostersAdapter extends RecyclerView.Adapter<PostersAdapter.PostersViewHolder>{

    private final String TAG = this.getClass().getSimpleName();

    private int mResource;
    private boolean mShouldAttachToParentImmediately = false;
    private Context mContext;
    private List<Movie> mPosters;

    private ClickListener mListener;

    public PostersAdapter(Context context, int resource, List<Movie> listOfPosters) {
        mContext = context;
        mResource = resource;
        mPosters = listOfPosters;
    }

    public void setPosterListener(ClickListener listener) {
        mListener = listener;
    }

    @Override
    public PostersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mResource, parent, mShouldAttachToParentImmediately);
        return new PostersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostersViewHolder holder, int position) {
        String fullPosterPath = TMDBHelper.getFullPosterPath(
                mPosters.get(position).getPosterPath()
        );

        Log.e(TAG, fullPosterPath);

        Picasso.with(mContext)
                .load(fullPosterPath)
                .into(holder.mIvPoster);
    }

    @Override
    public int getItemCount() {
        return mPosters.size();
    }

    class PostersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView mIvPoster;

        public PostersViewHolder(View itemView) {
            super(itemView);
            mIvPoster = itemView.findViewById(R.id.iv_poster);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(getAdapterPosition());
        }
    }
}
