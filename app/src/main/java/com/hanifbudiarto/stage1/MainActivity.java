package com.hanifbudiarto.stage1;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hanifbudiarto.stage1.data.local.MovieContract;
import com.hanifbudiarto.stage1.data.movie.GetMovies;
import com.hanifbudiarto.stage1.data.movie.Movie;
import com.hanifbudiarto.stage1.display.ClickListener;
import com.hanifbudiarto.stage1.display.posters.PostersAdapter;
import com.hanifbudiarto.stage1.utils.ConnectivityUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private RecyclerView mPosters;
    private GridLayoutManager mLayoutManager;

    private PostersAdapter mAdapter;
    private List<Movie> mMovies;

    private String mCurrentMovieCategory;
    private Parcelable mListState;

    private ProgressBar mPbLoading;
    private boolean mLoadingPosters = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentMovieCategory = savedInstanceState.getString(getString(R.string.state_movie_category));
            mListState = savedInstanceState.getParcelable(getString(R.string.state_poster_list));
        }
        setContentView(R.layout.activity_main);

        mMovies = new ArrayList<>();
        mAdapter = new PostersAdapter(this, R.layout.poster_list_item, mMovies);
        mAdapter.setPosterListener(onPosterClicked);

        mPosters = (RecyclerView) findViewById(R.id.rv_posters);
        mPosters.setAdapter(mAdapter);

        mLayoutManager = new GridLayoutManager(this, 3);
        mPosters.setLayoutManager(mLayoutManager);

        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);
    }

    @Override
    protected void onResume() {
        super.onResume();
        populatePosters();
    }

    private void populatePosters() {
        if (mCurrentMovieCategory != null && mCurrentMovieCategory.equals(getString(R.string.top_rated)))
            getTopRatedMovies();
        else if (mCurrentMovieCategory != null && mCurrentMovieCategory.equals(getString(R.string.favorites)))
            getFavoriteMovies();
        else getMostPopularMovies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movies_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.most_popular:
                getMostPopularMovies();
                return true;
            case R.id.top_rated:
                getTopRatedMovies();
                return true;
            case R.id.favorites:
                getFavoriteMovies();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save movie category state
        outState.putString(getString(R.string.state_movie_category), mCurrentMovieCategory);

        // save list state
        Parcelable listState = mLayoutManager.onSaveInstanceState();
        outState.putParcelable(getString(R.string.state_poster_list), listState);
    }

    private void getMostPopularMovies() {
        if (isLoadingPosters()) return;

        String mostPopularStr = getString(R.string.most_popular);
        mCurrentMovieCategory = mostPopularStr;
        setTitle(mostPopularStr);
        getMoviePosters(mostPopularStr);
    }

    private void getTopRatedMovies() {
        if (isLoadingPosters()) return;

        String topRatedStr = getString(R.string.top_rated);
        mCurrentMovieCategory = topRatedStr;
        setTitle(topRatedStr);
        getMoviePosters(topRatedStr);
    }

    private void getFavoriteMovies() {
        if (isLoadingPosters()) return;

        mLoadingPosters = false;
        notifyLoading();

        String favoriteStr = getString(R.string.favorites);
        mCurrentMovieCategory = favoriteStr;
        setTitle(favoriteStr);

        List<Movie> movieList = getAllFavorites();
        mMovies.clear();
        mMovies.addAll(movieList);
        mAdapter.notifyDataSetChanged();
    }

    private void getMoviePosters(String movieCategory) {
        if (!ConnectivityUtils.isOnline(this)) {
            Toast.makeText(
                    this, getString(R.string.offline_message), Toast.LENGTH_SHORT
            ).show();
            return;
        }

        mLoadingPosters = true;
        notifyLoading();

        new GetMovies(this)
                .setListener(onFinishedGetMovies)
                .execute(movieCategory);
    }

    private GetMovies.GetMoviesListener onFinishedGetMovies = new GetMovies.GetMoviesListener() {
        @Override
        public void onFinished(List<Movie> movieList) {
            mLoadingPosters = false;
            notifyLoading();

            if (movieList != null) {
                mMovies.clear();
                mMovies.addAll(movieList);

                mAdapter.notifyDataSetChanged();
            }

            if(mListState != null) {
                mLayoutManager.onRestoreInstanceState(mListState);
                mListState = null;
            }
        }
    };

    private ClickListener onPosterClicked = new ClickListener() {
        @Override
        public void onClick(int index) {
            Movie movie = mMovies.get(index);
            Log.i(TAG, movie.getId());

            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
            intent.putExtra(getString(R.string.movie_key), movie);

            PackageManager packageManager = getPackageManager();
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent);
            }
        }
    };

    private boolean isLoadingPosters() {
        return mLoadingPosters;
    }

    private void notifyLoading() {
        mPbLoading.setVisibility(
                isLoadingPosters() ? View.VISIBLE : View.GONE
        );
    }

    private List<Movie> getAllFavorites(){
        List<Movie> favoriteList = new ArrayList<>();

        Cursor cursor = getContentResolver().query(
                MovieContract.MovieEntry.CONTENT_URI, null, null, null, MovieContract.MovieEntry.COLUMN_TIMESTAMP
        );

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Movie movie = new Movie(
                        cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_MOVIE_ID)),
                        cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_RELEASE_DATE)),
                        cursor.getDouble(cursor.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_RATING)),
                        cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_SYNOPSIS)),
                        cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_POSTER))
                );

                favoriteList.add(movie);
                cursor.moveToNext();
            }
        }
        return favoriteList;
    }
}
