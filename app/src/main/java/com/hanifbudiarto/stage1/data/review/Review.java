package com.hanifbudiarto.stage1.data.review;

/**
 * Created by Hanif on 7/30/2017.
 */

public class Review {
    private String author, content, url;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
