package com.hanifbudiarto.stage1.data.movie;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.hanifbudiarto.stage1.R;
import com.hanifbudiarto.stage1.api.tmdb.config.TMDBConfig;
import com.hanifbudiarto.stage1.api.tmdb.property.TMDBJson;
import com.hanifbudiarto.stage1.utils.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hanif on 7/1/2017.
 */

public class GetMovies extends AsyncTask<String, Void, List<Movie>> {

    private final String TAG = getClass().getSimpleName();
    private GetMoviesListener mListener = null;
    private Context mContext;

    public GetMovies(Context context) {
        this.mContext = context;
    }

    @Override
    protected List<Movie> doInBackground(String... categories) {
        List<Movie> movies = new ArrayList<>();
        if (categories.length < 1) return movies;

        URL url = NetworkUtils.buildUrl(getUri(categories[0]));
        try {
            String jsonResult = NetworkUtils.getResponseFromHttpUrl(url);
            movies = getMoviesFromJson(jsonResult);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return movies;
    }

    @Override
    protected void onPostExecute(List<Movie> movies) {
        if (mListener != null) mListener.onFinished(movies);
    }

    private List<Movie> getMoviesFromJson(String json) {
        Log.i(TAG, json);

        List<Movie> movies = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray results = jsonObject.getJSONArray(TMDBJson.RESULT);

            for (int i=0; i<results.length(); i++) {
                JSONObject obj = results.getJSONObject(i);

                Movie movie = new Movie(
                        obj.getString(TMDBJson.ID),
                        obj.getString(TMDBJson.ORI_TITLE),
                        obj.getString(TMDBJson.REL_DATE),
                        obj.getDouble(TMDBJson.RATING),
                        obj.getString(TMDBJson.OVERVIEW),
                        obj.getString(TMDBJson.POSTER)
                );

                movies.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return movies;
    }

    private Uri getUri(String category) {
        String base = TMDBConfig.MOVIEDB_BASE_URL;
        base += (category.equals(mContext.getString(R.string.most_popular))) ? TMDBConfig.MOST_POPULAR_ENDPOINT
                : TMDBConfig.TOP_RATED_ENDPOINT;

        Uri builtUri = Uri.parse(base).buildUpon()
                .appendQueryParameter(TMDBConfig.PARAM_API_KEY, TMDBConfig.API_KEY)
                .build();
        return builtUri;
    }

    public GetMovies setListener(GetMoviesListener listener) {
        mListener = listener;
        return this;
    }

    public interface GetMoviesListener {
        void onFinished(List<Movie> movieList);
    }
}
