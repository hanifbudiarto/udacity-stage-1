package com.hanifbudiarto.stage1.data.trailer;

/**
 * Created by Hanif on 7/29/2017.
 */

public class Trailer {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
