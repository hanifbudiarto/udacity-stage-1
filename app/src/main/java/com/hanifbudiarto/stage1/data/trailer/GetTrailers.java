package com.hanifbudiarto.stage1.data.trailer;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.hanifbudiarto.stage1.api.tmdb.config.TMDBConfig;
import com.hanifbudiarto.stage1.api.tmdb.property.TMDBJson;
import com.hanifbudiarto.stage1.utils.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hanif on 7/29/2017.
 */

public class GetTrailers extends AsyncTask<String, Void, List<Trailer>>{

    private final String TAG = getClass().getSimpleName();
    private GetTrailersListener mListener = null;

    @Override
    protected List<Trailer> doInBackground(String... moviesId) {
        List<Trailer> trailers = new ArrayList<>();

        if (moviesId.length < 1) return trailers;

        URL url = NetworkUtils.buildUrl(getUri(moviesId[0]));
        try {
            String jsonResult = NetworkUtils.getResponseFromHttpUrl(url);
            Log.e(TAG, jsonResult);
            trailers = getTrailersFromJson(jsonResult);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return trailers;
    }

    @Override
    protected void onPostExecute(List<Trailer> trailers) {
        if (mListener != null) mListener.onFinished(trailers);
    }

    private List<Trailer> getTrailersFromJson(String json) {
        List<Trailer> trailers = new ArrayList<>();

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(json);
            JSONArray results = jsonObject.getJSONArray(TMDBJson.RESULT);

            for (int i=0; i<results.length(); i++) {
                JSONObject obj = results.getJSONObject(i);

                Trailer trailer = new Trailer();
                trailer.setKey(obj.getString(TMDBJson.YT_SOURCE));

                trailers.add(trailer);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return trailers;
    }

    private Uri getUri(String movieId) {
        String base = TMDBConfig.MOVIEDB_BASE_URL + "/" + movieId + TMDBConfig.VIDEO_ENDPOINT;
        Log.e(TAG, base);

        Uri builtUri = Uri.parse(base).buildUpon()
                .appendQueryParameter(TMDBConfig.PARAM_API_KEY, TMDBConfig.API_KEY)
                .build();
        return builtUri;
    }


    public GetTrailers setListener(GetTrailersListener listener) {
        mListener = listener;
        return this;
    }

    public interface GetTrailersListener {
        void onFinished(List<Trailer> trailers);
    }
}
