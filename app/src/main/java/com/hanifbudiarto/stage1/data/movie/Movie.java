package com.hanifbudiarto.stage1.data.movie;

import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.hanifbudiarto.stage1.api.tmdb.helper.TMDBHelper;
import com.squareup.picasso.Picasso;

/**
 * Created by Hanif on 7/1/2017.
 */

public class Movie implements Parcelable {
    private String id;
    private String originalTitle, posterPath, releaseDate, synopsis;
    private double userRating;

    public Movie(String id, String title, String release, double rating, String synopsis, String poster) {
        setId(id);
        setOriginalTitle(title);
        setReleaseDate(release);
        setUserRating(rating);
        setSynopsis(synopsis);
        setPosterPath(poster);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public double getUserRating() {
        return userRating;
    }

    public void setUserRating(double userRating) {
        this.userRating = userRating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }


    // Parcelling Part
    protected Movie(Parcel in) {
        String[] data = new String[6];
        in.readStringArray(data);

        this.setOriginalTitle(data[0]);
        this.setReleaseDate(data[1]);
        this.setUserRating(Double.parseDouble(data[2]));
        this.setSynopsis(data[3]);
        this.setPosterPath(data[4]);
        this.setId(data[5]);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[] {
                this.getOriginalTitle(),
                this.getReleaseDate(),
                String.valueOf(this.getUserRating()),
                this.getSynopsis(),
                this.getPosterPath(),
                this.getId(),
        });
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @BindingAdapter({"app:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        String fullPosterPath = TMDBHelper.getFullPosterPath(imageUrl);

        Picasso.with(view.getContext())
                .load(fullPosterPath)
                .into(view);
    }
}
