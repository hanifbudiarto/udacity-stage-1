package com.hanifbudiarto.stage1.data.review;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.hanifbudiarto.stage1.api.tmdb.config.TMDBConfig;
import com.hanifbudiarto.stage1.api.tmdb.property.TMDBJson;
import com.hanifbudiarto.stage1.utils.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hanif on 7/30/2017.
 */

public class GetReviews extends AsyncTask<String, Void, List<Review>> {

    private final String TAG = getClass().getSimpleName();
    private GetReviewsListener mListener;

    @Override
    protected List<Review> doInBackground(String... moviesId) {
        List<Review> reviews = new ArrayList<>();

        if (moviesId.length < 1) return reviews;

        URL url = NetworkUtils.buildUrl(getUri(moviesId[0]));
        try {
            String jsonResult = NetworkUtils.getResponseFromHttpUrl(url);
            Log.e(TAG, jsonResult);
            reviews = getReviewsFromJson(jsonResult);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return reviews;
    }

    private List<Review> getReviewsFromJson(String json) {
        List<Review> reviews = new ArrayList<>();

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(json);
            JSONArray results = jsonObject.getJSONArray(TMDBJson.RESULT);

            for (int i=0; i<results.length(); i++) {
                JSONObject obj = results.getJSONObject(i);

                Review review = new Review();
                review.setAuthor(obj.getString(TMDBJson.AUTHOR));
                review.setContent(obj.getString(TMDBJson.CONTENT));
                review.setUrl(obj.getString(TMDBJson.URL));

                reviews.add(review);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return reviews;
    }

    private Uri getUri(String movieId) {
        String base = TMDBConfig.MOVIEDB_BASE_URL + "/" + movieId + TMDBConfig.REVIEW_ENDPOINT;
        Log.e(TAG, base);

        Uri builtUri = Uri.parse(base).buildUpon()
                .appendQueryParameter(TMDBConfig.PARAM_API_KEY, TMDBConfig.API_KEY)
                .build();
        return builtUri;
    }

    @Override
    protected void onPostExecute(List<Review> reviews) {
        if (mListener != null) {
            mListener.onFinished(reviews);
        }
    }

    public GetReviews setListener(GetReviewsListener listener) {
        this.mListener = listener;
        return this;
    }

    public interface GetReviewsListener{
        void onFinished(List<Review> reviews);
    }
}
