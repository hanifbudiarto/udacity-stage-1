package com.hanifbudiarto.stage1.api.tmdb.property;

/**
 * Created by Hanif on 7/1/2017.
 */

public class TMDBJson {

    // https://api.themoviedb.org/3/movie/<<popular//top_rated>>
    public static final String RESULT = "results";
    public static final String ID = "id";
    public static final String ORI_TITLE = "original_title";
    public static final String REL_DATE = "release_date";
    public static final String POSTER = "poster_path";
    public static final String OVERVIEW = "overview";
    public static final String RATING = "vote_average";

    // https://api.themoviedb.org/3/movie/<<movie_id>>/trailers
    public static final String YT_SOURCE = "key";

    // https://api.themoviedb.org/3/movie/<<movie_id>>/reviews
    public static final String AUTHOR = "author";
    public static final String CONTENT = "content";
    public static final String URL = "url";
}
