package com.hanifbudiarto.stage1.api.tmdb.config;

/**
 * Created by Hanif on 7/1/2017.
 */

public class TMDBConfig {

    public static final String BASE_IMG_URL = "http://image.tmdb.org/t/p/";
    public static final String IMG_SIZE = "w185";

    public static final String PARAM_API_KEY = "api_key";
    public static final String API_KEY = "5575d5147e31f25ccbc2512e880df903";

    public static final String MOVIEDB_BASE_URL = "http://api.themoviedb.org/3/movie";
    public static final String MOST_POPULAR_ENDPOINT = "/popular";
    public static final String TOP_RATED_ENDPOINT = "/top_rated";

    // concat with movie id
    public static final String VIDEO_ENDPOINT = "/videos";
    public static final String REVIEW_ENDPOINT = "/reviews";

}
