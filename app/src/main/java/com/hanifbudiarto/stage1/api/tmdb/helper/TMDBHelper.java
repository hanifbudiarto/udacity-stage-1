package com.hanifbudiarto.stage1.api.tmdb.helper;

import com.hanifbudiarto.stage1.api.tmdb.config.TMDBConfig;

/**
 * Created by Hanif on 7/29/2017.
 */

public class TMDBHelper {

    public static String getFullPosterPath(String imgName) {
        return TMDBConfig.BASE_IMG_URL + TMDBConfig.IMG_SIZE + imgName;
    }
}
