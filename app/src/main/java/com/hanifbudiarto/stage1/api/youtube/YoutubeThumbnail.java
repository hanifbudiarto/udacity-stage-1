package com.hanifbudiarto.stage1.api.youtube;

/**
 * Created by Hanif on 7/29/2017.
 */

public class YoutubeThumbnail {

    // /<<video_id>>/0.jpg
    private static final String base = "https://img.youtube.com/vi";

    public static String getImage(String videoId) {
        return base + "/" + videoId + "/" +"0.jpg";
    }
}
