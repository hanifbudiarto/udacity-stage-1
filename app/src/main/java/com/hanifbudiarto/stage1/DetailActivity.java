package com.hanifbudiarto.stage1;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hanifbudiarto.stage1.data.local.MovieContract;
import com.hanifbudiarto.stage1.data.movie.Movie;
import com.hanifbudiarto.stage1.data.review.GetReviews;
import com.hanifbudiarto.stage1.data.review.Review;
import com.hanifbudiarto.stage1.data.trailer.GetTrailers;
import com.hanifbudiarto.stage1.data.trailer.Trailer;
import com.hanifbudiarto.stage1.databinding.ActivityDetailBinding;
import com.hanifbudiarto.stage1.display.ClickListener;
import com.hanifbudiarto.stage1.display.reviews.ReviewsAdapter;
import com.hanifbudiarto.stage1.display.thumbnail.ThumbnailsAdapter;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private LinearLayout mLlLoadingTrailers;
    private boolean mLoadingTrailers = false;

    private LinearLayout mLlLoadingReviews;
    private boolean mLoadingReviews = false;

    private RecyclerView mThumbnails;
    private LinearLayoutManager mThumbnailsLayoutManager;
    private ThumbnailsAdapter mThumbnailsAdapter;
    private List<Trailer> mTrailers;

    private RecyclerView mComments;
    private LinearLayoutManager mCommentsLayoutManager;
    private ReviewsAdapter mReviewsAdapter;
    private List<Review> mReviews;

    private FloatingActionButton mFabFavorite;
    private Movie mCurrentMovie;

    private boolean mFavorite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityDetailBinding activityDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        Bundle data = getIntent().getExtras();
        Movie movie = data.getParcelable(getString(R.string.movie_key));
        setCurrentMovie(movie);

        activityDetailBinding.setMovie(movie);
        activityDetailBinding.setActivity(this);

        getTrailers(movie.getId());
        getReviews(movie.getId());

        checkFavorite(movie.getId());
    }

    private void checkFavorite(String movieId) {
        String[] projection = {MovieContract.MovieEntry.COLUMN_MOVIE_ID};
        String selectionClause = MovieContract.MovieEntry.COLUMN_MOVIE_ID + " = ?";
        String[] selectionArgs = {""};
        selectionArgs[0] = movieId;

        Cursor cursor = getContentResolver().query(
                MovieContract.MovieEntry.CONTENT_URI,
                projection,
                selectionClause,
                selectionArgs,
                MovieContract.MovieEntry.COLUMN_TIMESTAMP
        );

        if (cursor.getCount() == 0) {
            mFavorite = false;
            mFabFavorite.setImageResource(R.mipmap.ic_favorite_border_white_48dp);
        }
        else {
            mFavorite = true;
            mFabFavorite.setImageResource(R.mipmap.ic_favorite_white_48dp);
        }
    }

    private void openLink(String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        PackageManager packageManager = getPackageManager();
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent);
        }
    }

    private void init() {
        mTrailers = new ArrayList<>();

        mThumbnailsAdapter = new ThumbnailsAdapter(this, R.layout.thumbnail_list_item, mTrailers);
        mThumbnailsAdapter.setThumbnailListener(new ClickListener() {
            @Override
            public void onClick(int index) {
                openLink("http://www.youtube.com/watch?v=" + mTrailers.get(index).getKey());
            }
        });
        mThumbnailsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        mThumbnails = (RecyclerView) findViewById(R.id.rv_thumbnails);
        mThumbnails.setNestedScrollingEnabled(false);
        mThumbnails.setLayoutManager(mThumbnailsLayoutManager);
        mThumbnails.setAdapter(mThumbnailsAdapter);



        mReviews = new ArrayList<>();

        mReviewsAdapter = new ReviewsAdapter(this, R.layout.review_list_item, mReviews);
        mReviewsAdapter.setReviewsListener(new ClickListener() {
            @Override
            public void onClick(int index) {
                openLink(mReviews.get(index).getUrl());
            }
        });
        mCommentsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mComments = (RecyclerView) findViewById(R.id.rv_comments);
        mComments.setNestedScrollingEnabled(false);
        mComments.setLayoutManager(mCommentsLayoutManager);
        mComments.setAdapter(mReviewsAdapter);

        mLlLoadingReviews = (LinearLayout) findViewById(R.id.ll_loading_reviews);
        mLlLoadingTrailers = (LinearLayout) findViewById(R.id.ll_loading_trailers);

        mFabFavorite = (FloatingActionButton) findViewById(R.id.fab_favorite);
        mFabFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFavorite == false) {
                    boolean added = addMovieAsFavorites(mCurrentMovie);
                    if (added) Toast.makeText(getApplicationContext(), "Added to my favorites", Toast.LENGTH_SHORT).show();
                }
                else {
                    boolean removed = removeMovieFromFavorites(mCurrentMovie.getId());
                    if (removed) Toast.makeText(getApplicationContext(), "Removed from my favorites", Toast.LENGTH_SHORT).show();
                }
                checkFavorite(mCurrentMovie.getId());
            }
        });

    }

    private void setCurrentMovie(Movie movie) {
        this.mCurrentMovie = movie;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getReviews(String movieId) {
        mLoadingReviews = true;
        notifyLoading();

        new GetReviews()
                .setListener(new GetReviews.GetReviewsListener() {
                    @Override
                    public void onFinished(List<Review> reviews) {
                        mLoadingReviews = false;
                        notifyLoading();

                        mReviews.clear();
                        mReviews.addAll(reviews);
                        mReviewsAdapter.notifyDataSetChanged();
                    }
                })
                .execute(movieId);
    }

    private void getTrailers(String movieId) {
        mLoadingTrailers = true;
        notifyLoading();

        new GetTrailers()
                .setListener(new GetTrailers.GetTrailersListener() {
                    @Override
                    public void onFinished(List<Trailer> trailers) {
                        mLoadingTrailers = false;
                        notifyLoading();

                        mTrailers.clear();
                        mTrailers.addAll(trailers);
                        mThumbnailsAdapter.notifyDataSetChanged();
                    }
                })
                .execute(movieId);
    }

    private void notifyLoading() {

        mLlLoadingTrailers.setVisibility(
                mLoadingTrailers ? View.VISIBLE : View.GONE
        );

        mLlLoadingReviews.setVisibility(
                mLoadingReviews ? View.VISIBLE : View.GONE
        );
    }

    private boolean addMovieAsFavorites(Movie movie) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MovieContract.MovieEntry.COLUMN_MOVIE_ID, movie.getId());
        contentValues.put(MovieContract.MovieEntry.COLUMN_TITLE, movie.getOriginalTitle());
        contentValues.put(MovieContract.MovieEntry.COLUMN_POSTER, movie.getPosterPath());
        contentValues.put(MovieContract.MovieEntry.COLUMN_RELEASE_DATE, movie.getReleaseDate());
        contentValues.put(MovieContract.MovieEntry.COLUMN_SYNOPSIS, movie.getSynopsis());
        contentValues.put(MovieContract.MovieEntry.COLUMN_RATING, movie.getUserRating());

        Uri uri = getContentResolver().insert(MovieContract.MovieEntry.CONTENT_URI, contentValues);

        return uri != null;
    }

    private boolean removeMovieFromFavorites(String movieId) {

        String selectionClause = MovieContract.MovieEntry.COLUMN_MOVIE_ID + " = ?";
        String[] selectionArgs = {""};
        selectionArgs[0] = movieId;

        int delCount = getContentResolver().delete(
                            MovieContract.MovieEntry.CONTENT_URI,
                            selectionClause,
                            selectionArgs
                        );
        return delCount > 0;
    }

}
